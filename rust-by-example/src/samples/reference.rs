fn main() {
  let x = 1;
  let y = &x;
  println!("y: {}", y);
  println!("*y: {}", *y);
  let mut a = 2;
  let b = &mut a;
  *b = 2;
  println!("b: {}", b);
  println!("*b: {}", *b);
  let mut c = &b;
  println!("c: {}", c);
  println!("*c: {}", *c);

}
