	.section	__TEXT,__text,regular,pure_instructions
	.private_extern	__ZN3std2rt10lang_start17hb9bce882b6c930b4E
	.globl	__ZN3std2rt10lang_start17hb9bce882b6c930b4E
	.p2align	4, 0x90
__ZN3std2rt10lang_start17hb9bce882b6c930b4E:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, -8(%rbp)
	leaq	l_vtable.0(%rip), %rsi
	leaq	-8(%rbp), %rdi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	__ZN3std2rt19lang_start_internal17h336cd1da103a02e8E
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.p2align	4, 0x90
__ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hb692deac74bbcdfcE:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	*(%rdi)
	popq	%rbp
	jmp	__ZN54_$LT$$LP$$RP$$u20$as$u20$std..process..Termination$GT$6report17h25a392e841746f3fE
	.cfi_endproc

	.p2align	4, 0x90
__ZN4core3ops8function6FnOnce9call_once17h70632e1c68af082bE:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	*%rdi
	popq	%rbp
	jmp	__ZN54_$LT$$LP$$RP$$u20$as$u20$std..process..Termination$GT$6report17h25a392e841746f3fE
	.cfi_endproc

	.p2align	4, 0x90
__ZN4core3ptr13drop_in_place17hba694bb64f4e15ffE:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.p2align	4, 0x90
__ZN10square_sum4main17h8bc5570877fbc53fE:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	leaq	-8(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	$120, -8(%rbp)
	movq	__ZN4core3fmt3num54_$LT$impl$u20$core..fmt..Display$u20$for$u20$isize$GT$3fmt17h9d46e6c2ddad6d8cE@GOTPCREL(%rip), %rax
	movq	%rax, -16(%rbp)
	leaq	l_byte_str.8(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	$2, -64(%rbp)
	leaq	l_byte_str.9(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	$1, -48(%rbp)
	leaq	-24(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	$1, -32(%rbp)
	leaq	-72(%rbp), %rdi
	callq	__ZN3std2io5stdio6_print17h15473476d9227c7fE
	addq	$80, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_main
	.p2align	4, 0x90
_main:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rsi, %rax
	movslq	%edi, %rdx
	leaq	__ZN10square_sum4main17h8bc5570877fbc53fE(%rip), %rcx
	movq	%rcx, -8(%rbp)
	leaq	l_vtable.0(%rip), %rsi
	leaq	-8(%rbp), %rdi
	movq	%rax, %rcx
	callq	__ZN3std2rt19lang_start_internal17h336cd1da103a02e8E
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__DATA,__const
	.p2align	3
l_vtable.0:
	.quad	__ZN4core3ptr13drop_in_place17hba694bb64f4e15ffE
	.quad	8
	.quad	8
	.quad	__ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hb692deac74bbcdfcE
	.quad	__ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hb692deac74bbcdfcE
	.quad	__ZN4core3ops8function6FnOnce9call_once17h70632e1c68af082bE

	.section	__TEXT,__const
l_byte_str.6:
	.byte	0

l_byte_str.7:
	.byte	10

	.section	__DATA,__const
	.p2align	3
l_byte_str.8:
	.quad	l_byte_str.6
	.space	8
	.quad	l_byte_str.7
	.asciz	"\001\000\000\000\000\000\000"

	.section	__TEXT,__const
	.p2align	3
l_byte_str.9:
	.asciz	"\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\003\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\003\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000 \000\000\000\000\000\000\000\003\000\000\000\000\000\000"


.subsections_via_symbols
