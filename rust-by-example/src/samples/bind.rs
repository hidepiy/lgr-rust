fn main() {
  let x = 1 + 0;
  let mut y = x;
  println!("----------");
  println!("1 x: {}", x);
  println!("1 y: {}", y);
  let x = 2;
  println!("----------");
  println!("2 x: {}", x);
  println!("2 y: {}", y);
  let z = y;
  println!("----------");
  println!("3 x: {}", x);
  println!("3 y: {}", y);
  println!("3 z: {}", z);
  y = 4;
  println!("----------");
  println!("4 x: {}", x);
  println!("4 y: {}", y);
  println!("4 z: {}", z);
}

