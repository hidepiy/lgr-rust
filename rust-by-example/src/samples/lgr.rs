fn main() {
    println!("Let's get Rocking!");
    println!("{0} {1} {a}", "Zero", "One", a="Two");
    println!("{} of {:b} people know binary, the other half doesn't", 1, 2);
    println!("{number:>width$}", number=1, width=5);
    println!("{number:>0width$}", number=1, width=5);
    let pi = 3.141592;
    println!("Pi is roughly {:.*}", 3, pi);
}
