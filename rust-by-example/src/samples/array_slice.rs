fn main() {
  let a: [isize; 3] = [1, 2, 3];
  println!("array a: {:?}", a);
  let b: &[isize] = &a;
  println!("slice b: {:?}", b);
  for elm in b {
    println!("{}", elm);
  }
}
