fn rebind(n: isize) {
  let sum = 100000;
  for i in 0..n {
    let sum = sum + i;
    println!("rebind current sum: {}", sum);
  }
  println!("rebind: {}", sum);
}

fn reassign(n: isize) {
  let mut sum = 100000;
  for i in 0..n {
    sum = sum + i;
    println!("reassign current sum: {}", sum);
  }
  println!("reassign: {}", sum);
}

fn main() {
  rebind(5);
  reassign(5);
}

