
fn lgr() {
    println!("Let's get Rocking!")
}

fn main() {
    let ret = lgr();
    assert_eq!(ret, ());
    assert_eq!(std::mem::size_of::<()>(), 0);
    assert_eq!(std::mem::size_of::<bool>(), 1);
}
