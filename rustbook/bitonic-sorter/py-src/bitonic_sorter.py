
def sort(naturals, is_ascend):
    if len(naturals) <= 1:
        return naturals
    else:
        mid_point = len(naturals) //2
        former_half = sort(naturals[:mid_point], True)
        latter_half = sort(naturals[mid_point:], False)
        return _sub_sort(former_half + latter_half, is_ascend)

def _sub_sort(naturals, is_ascend):
    if len(naturals) == 1:
        return naturals
    else:
        mid_point = len(naturals) // 2
        for i in range(mid_point):
            if (naturals[i] > naturals[i + mid_point]) == is_ascend:
                naturals[i], naturals[i + mid_point] = naturals[i + mid_point], naturals[i]
    former_half = _sub_sort(naturals[:mid_point], is_ascend)
    latter_half = _sub_sort(naturals[mid_point:], is_ascend)
    return former_half + latter_half

