pub fn sort(naturals: &mut [u32], is_ascend: bool) {
    if naturals.len() > 1 {
        let mid_point = naturals.len() / 2;
        sort(&mut naturals[..mid_point], true);
        sort(&mut naturals[mid_point..], false);
        sub_sort(naturals, is_ascend);
    }
}

fn sub_sort(naturals: &mut [u32], is_ascend: bool) {
    if naturals.len() > 1 {
        let mid_point = naturals.len() / 2;
        for i in 0..mid_point {
            if (naturals[i] > naturals[i + mid_point]) == is_ascend {
                naturals.swap(i, i + mid_point);
            }
        }
        sub_sort(&mut naturals[..mid_point], is_ascend);
        sub_sort(&mut naturals[mid_point..], is_ascend);
    }
}

#[cfg(test)]
mod tests {
    use super::sort;
    #[test]
    fn sort_u32_ascending() {
        let mut naturals = vec![10, 30, 11, 20, 4, 330, 21, 110];
        sort(&mut naturals, true);
        assert_eq!(naturals, vec![4, 10, 11, 20, 21, 30, 110, 330]);
    }

    #[test]
    fn sort_u32_dscending() {
        let mut naturals = vec![10, 30, 11, 20, 4, 330, 21, 110];
        sort(&mut naturals, false);
        assert_eq!(naturals, vec![330, 110, 30, 21, 20, 11, 10, 4]);
    }
}
