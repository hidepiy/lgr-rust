use super::SortOrder;
pub fn sort<T: Ord>(x: &mut [T], order: &SortOrder) -> Result<(), String> {
    if x.len().is_power_of_two() {
        match *order {
            SortOrder::Ascending => do_sort(x, true),
            SortOrder::Descending => do_sort(x, false),
        };
        Ok(())
    } else {
        Err(format!("The length of x should be power of two. (x.len(): {})", x.len()))
    }
}

fn do_sort<T: Ord>(x: &mut [T], is_ascend: bool) {
    if x.len() > 1 {
        let mid_point = x.len() / 2;
        do_sort(&mut x[..mid_point], true);
        do_sort(&mut x[mid_point..], false);
        sub_sort(x, is_ascend);
    }
}

fn sub_sort<T: Ord>(x: &mut [T], is_ascend: bool) {
    if x.len() > 1 {
        let mid_point = x.len() / 2;
        for i in 0..mid_point {
            if (x[i] > x[i + mid_point]) == is_ascend {
                x.swap(i, i + mid_point);
            }
        }
        sub_sort(&mut x[..mid_point], is_ascend);
        sub_sort(&mut x[mid_point..], is_ascend);
    }
}

#[cfg(test)]
mod tests {
    use super::sort;
    use crate::SortOrder::*;
    
    #[test]
    fn sort_u32_ascending() {
        let mut naturals: Vec<u32> = vec![10, 30, 11, 20, 4, 330, 21, 110];
        assert_eq!(sort(&mut naturals, &Ascending), Ok(()));
        assert_eq!(naturals, vec![4, 10, 11, 20, 21, 30, 110, 330]);
    }

    #[test]
    fn sort_u32_dscending() {
        let mut naturals: Vec<u32> = vec![10, 30, 11, 20, 4, 330, 21, 110];
        assert_eq!(sort(&mut naturals, &Descending), Ok(()));
        assert_eq!(naturals, vec![330, 110, 30, 21, 20, 11, 10, 4]);
    }

    #[test]
    fn sort_str_ascending() {
        let mut strings = vec!["Rust", "is", "fast", "and", "memory-efficient", "with", "no", "GC"];
        assert_eq!(sort(&mut strings, &Ascending), Ok(()));
        assert_eq!(strings, vec!["GC", "Rust", "and", "fast", "is", "memory-efficient", "no", "with"]);
    }

    #[test]
    fn sort_str_descending() {
        let mut strings = vec!["Rust", "is", "fast", "and", "memory-efficient", "with", "no", "GC"];
        assert_eq!(sort(&mut strings, &Descending), Ok(()));
        assert_eq!(strings, vec!["with", "no", "memory-efficient", "is", "fast", "and", "Rust", "GC"]);
    }

    #[test]
    fn sort_to_fail() {
        let mut x = vec![1,2,3];
        assert!(sort(&mut x, &Ascending).is_err());
    }

}
