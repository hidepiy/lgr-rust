use super::SortOrder;
use std::cmp::Ordering;
use std::cmp::Ordering::*;
use rayon;

const PARALLEL_THRESHOLD: usize = 4098;

pub fn sort<T: Ord + Send>(x: &mut [T], order: &SortOrder) -> Result<(), String> {
    match *order {
        SortOrder::Ascending => sort_by(x, &|a, b| a.cmp(b)),
        SortOrder::Descending => sort_by(x, &|a, b| b.cmp(a)),
    }
}

pub fn sort_by<T, F>(x: &mut [T], comparator: &F) -> Result<(), String>
    where T: Send, F: Sync + Fn(&T, &T) -> Ordering {
    if x.len().is_power_of_two() {
        do_sort(x, Greater, comparator);
        Ok(())
    } else {
        Err(format!("The length of x should be power of two. (x.len(): {})", x.len()))
    }
}

fn do_sort<T, F>(x: &mut [T], order: Ordering, comparator: &F)
    where T: Send, F: Sync + Fn(&T, &T) -> Ordering {
    if x.len() > 1 {
        let mid_point = x.len() / 2;
        let (former, latter) = x.split_at_mut(mid_point);
        if mid_point >= PARALLEL_THRESHOLD {
            rayon::join(|| do_sort(former, Greater, comparator),
                        || do_sort(latter, Less, comparator));
        } else {
            do_sort(former, Greater, comparator);
            do_sort(latter, Less, comparator);
        }
        sub_sort(x, order, comparator);
   }
}

fn sub_sort<T, F>(x: &mut [T], order: Ordering, comparator:&F)
    where T: Send, F: Sync + Fn(&T, &T) -> Ordering {
    if x.len() > 1 {
        let mid_point = x.len() / 2;
        for i in 0..mid_point {
            if comparator(&x[i], &x[i + mid_point]) == order {
                x.swap(i, i + mid_point);
            }
        }
        let (former, latter) = x.split_at_mut(mid_point);
        if mid_point >= PARALLEL_THRESHOLD {
            rayon::join(|| sub_sort(former, order, comparator),
                        || sub_sort(latter, order, comparator));
        } else {
            sub_sort(former, order, comparator);
            sub_sort(latter, order, comparator);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{sort, sort_by};
    use crate::SortOrder::*;
    use crate::utils::{new_u32_vec, is_sorted_ascending, is_sorted_descending};

    #[derive(Debug, PartialEq)]
    struct Student {
        first_name: String,
        last_name: String,
        age: u8,
    }

    impl Student {
        fn new(first_name: &str, last_name:&str, age: u8) -> Self {
            Self {
                first_name: first_name.to_string(),
                last_name: last_name.to_string(),
                age,
            }
        }
    }

    #[test]
    fn sort_students_by_age_ascending() {
        let taro = Student::new("Taro", "Yamada", 16);
        let hanako = Student::new("Hanako", "Yamada", 14);
        let kyoko = Student::new("Kyoko", "Ito", 15);
        let ryosuke = Student::new("Ryosuke", "Hayashi", 17);
        let mut x = vec![&taro, &hanako, &kyoko, &ryosuke];
        let expected = vec![&hanako, &kyoko, &taro, &ryosuke];

        assert_eq!(
            sort_by(&mut x, &|a, b| a.age.cmp(&b.age))
            , Ok(())
        );
        assert_eq!(x, expected);
    }
    
    #[test]
    fn sort_students_by_name_ascending() {
        let taro = Student::new("Taro", "Yamada", 16);
        let hanako = Student::new("Hanako", "Yamada", 14);
        let kyoko = Student::new("Kyoko", "Ito", 15);
        let ryosuke = Student::new("Ryosuke", "Hayashi", 17);
        let mut x = vec![&taro, &hanako, &kyoko, &ryosuke];
        let expected = vec![&ryosuke, &kyoko, &hanako, &taro];

        assert_eq!(
            sort_by(&mut x,
                &|a, b| a.last_name.cmp(&b.last_name)
                .then_with(|| a.first_name.cmp(&b.first_name))
            )
            , Ok(())
        );
        assert_eq!(x, expected);
    }

    #[test]
    fn sort_u32_ascending() {
        let mut naturals: Vec<u32> = vec![10, 30, 11, 20, 4, 330, 21, 110];
        assert_eq!(sort(&mut naturals, &Ascending), Ok(()));
        assert_eq!(naturals, vec![4, 10, 11, 20, 21, 30, 110, 330]);
    }

    #[test]
    fn sort_u32_dscending() {
        let mut naturals: Vec<u32> = vec![10, 30, 11, 20, 4, 330, 21, 110];
        assert_eq!(sort(&mut naturals, &Descending), Ok(()));
        assert_eq!(naturals, vec![330, 110, 30, 21, 20, 11, 10, 4]);
    }

    #[test]
    fn sort_str_ascending() {
        let mut strings = vec!["Rust", "is", "fast", "and", "memory-efficient", "with", "no", "GC"];
        assert_eq!(sort(&mut strings, &Ascending), Ok(()));
        assert_eq!(strings, vec!["GC", "Rust", "and", "fast", "is", "memory-efficient", "no", "with"]);
    }

    #[test]
    fn sort_str_descending() {
        let mut strings = vec!["Rust", "is", "fast", "and", "memory-efficient", "with", "no", "GC"];
        assert_eq!(sort(&mut strings, &Descending), Ok(()));
        assert_eq!(strings, vec!["with", "no", "memory-efficient", "is", "fast", "and", "Rust", "GC"]);
    }

    #[test]
    fn sort_to_fail() {
        let mut x = vec![1,2,3];
        assert!(sort(&mut x, &Ascending).is_err());
    }

    #[test]
    fn sort_u32_large() {
        let mut x = new_u32_vec(65536);
        assert_eq!(sort(&mut x, &Ascending), Ok(()));
        assert!(is_sorted_ascending(&x));
        let mut x = new_u32_vec(65536);
        assert_eq!(sort(&mut x, &Descending), Ok(()));
        assert!(is_sorted_descending(&x));
    }


}
